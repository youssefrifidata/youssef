import { TestBed, inject } from '@angular/core/testing';

import { AuthetudiantService } from './authetudiant.service';

describe('AuthetudiantService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthetudiantService]
    });
  });

  it('should be created', inject([AuthetudiantService], (service: AuthetudiantService) => {
    expect(service).toBeTruthy();
  }));
});
