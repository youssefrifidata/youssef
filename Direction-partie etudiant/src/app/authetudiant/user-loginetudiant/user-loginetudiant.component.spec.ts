import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginetudiantComponent } from './user-loginetudiant.component';

describe('UserLoginetudiantComponent', () => {
  let component: UserLoginetudiantComponent;
  let fixture: ComponentFixture<UserLoginetudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginetudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginetudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
