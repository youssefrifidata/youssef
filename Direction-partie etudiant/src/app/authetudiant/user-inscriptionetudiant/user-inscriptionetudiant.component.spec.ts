import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInscriptionetudiantComponent } from './user-inscriptionetudiant.component';

describe('UserInscriptionetudiantComponent', () => {
  let component: UserInscriptionetudiantComponent;
  let fixture: ComponentFixture<UserInscriptionetudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInscriptionetudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInscriptionetudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
