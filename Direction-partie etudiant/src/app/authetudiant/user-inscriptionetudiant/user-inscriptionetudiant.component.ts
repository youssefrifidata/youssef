import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import {Router} from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-user-inscriptionetudiant',
  templateUrl: './user-inscriptionetudiant.component.html',
  styleUrls: ['./user-inscriptionetudiant.component.css']
})
export class UserInscriptionetudiantComponent implements OnInit {

  ville = ['ville'];
  niveaue = ['Niveau Etude'];
  filiere = ['Filiere'];
  showSuccessMessage: boolean;
  submitted: boolean;
  nom = '';
prenom = '';
daten = '';

email = '';

mobile = '';




  itemList: AngularFireList<any>;

  constructor(public db: AngularFireDatabase, private fire: AngularFireAuth
    , private router: Router, private titleService: Title) {
    this.itemList = db.list('etudiant');
  }

  myRegister() {
    this.submitted = true;


      this.showSuccessMessage = true;
      setTimeout(() => this.showSuccessMessage = false, 3000);
      this.submitted = false;

alert('Inscription reussite');

  this.itemList.push({
    nom: this.nom,
    prenom: this.prenom,
    daten:this.daten,
    email:this.email,
    mobile:this.mobile,
    filiere:this.filiere,
   ville:this.ville,

   niveaue:this.niveaue,


        });

  }


  ngOnInit() {
      this.setTitle('Creer un compte pour un etudiant ');

    }

    public setTitle( newTitle: string) {
      this.titleService.setTitle( newTitle );
  }

}
