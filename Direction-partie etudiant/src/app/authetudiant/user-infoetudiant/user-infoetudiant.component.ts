import { Component, OnInit } from '@angular/core';
import { AuthetudiantService } from '../authetudiant.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-user-infoetudiant',
  templateUrl: './user-infoetudiant.component.html',
  styleUrls: ['./user-infoetudiant.component.css']
})
export class UserInfoetudiantComponent implements OnInit {

  constructor(public authService: AuthetudiantService, private titleService: Title) {}
 
  ngOnInit() {
    this.setTitle('Espace Etudiant');

   }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}

 
  logout() {
    this.authService.signOut();
  }
 
}
