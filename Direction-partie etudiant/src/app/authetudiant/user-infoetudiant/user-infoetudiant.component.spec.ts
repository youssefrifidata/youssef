import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInfoetudiantComponent } from './user-infoetudiant.component';

describe('UserInfoetudiantComponent', () => {
  let component: UserInfoetudiantComponent;
  let fixture: ComponentFixture<UserInfoetudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInfoetudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfoetudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
