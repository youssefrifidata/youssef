import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursprofComponent } from './coursprof.component';

describe('CoursprofComponent', () => {
  let component: CoursprofComponent;
  let fixture: ComponentFixture<CoursprofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursprofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
