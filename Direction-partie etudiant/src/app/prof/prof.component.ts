import { Component, OnInit } from '@angular/core';
import { SharedprofService } from '../sharedprof/sharedprof.service';
import { AuthdirectionService } from '../authdirection/authdirection.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-prof',
  templateUrl: './prof.component.html',
  styleUrls: ['./prof.component.css']
})
export class ProfComponent implements OnInit {
  ville=["ville","CASABLACA","RABAT","TANGER"];
niveaue=["Niveau Etude","1er annee","2eme annee","3eme annee","4eme annee","5eme annee"];
matiere=["Matiere","C++","Math","Francais","Anglais"];
  constructor(private customerService: SharedprofService,private titleService: Title,public authService: AuthdirectionService) {   }
  submitted: boolean;
  showSuccessMessage: boolean;
  formControls = this.customerService.form.controls;
  ngOnInit() {
    this.setTitle('  Gestion des professeurs');

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}
  onSubmit() {
    this.submitted = true;
    if (this.customerService.form.valid) {
  if ( this.customerService.form.get('$key').value == null) {
        this.customerService.insertCustomer(this.customerService.form.value);
      } else {
        this.customerService.updateCustomer(this.customerService.form.value);
      }
      this.showSuccessMessage = true;
      setTimeout(() => this.showSuccessMessage = false, 3000);
      this.submitted = false;
      this.customerService.form.reset();
      this.customerService.form.setValue({
        $key: null,
        nom: '',
        prenom: '',
        cin: '',
        daten: '',
        email: '',
        matiere: '',
        mobile: '',
        pays: '',
        ville: '',
      });
    }
  }
  logout() {
    this.authService.signOut();
  }
}
