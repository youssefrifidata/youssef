import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private titleService: Title,private router: Router) { }


  espaceprof(){
    this.router.navigate(['/authprof/user-loginprof']);

  }

  ngOnInit() {
    this.setTitle('  Ecole Virtuelle');

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}
//   listeprofs() {
//     this.router.navigate(['./prof']);
//   }
//   listeetudiants() {
// this.router.navigate(['/Etudiant']);


//   }
//   registre(){
//     this.router.navigate(['/register']);
 
//   }
  
//   registerprof(){
//     this.router.navigate(['/registerprof']);
 
//   }
}
