import { Component, OnInit } from '@angular/core';
import { AuthdirectionService } from '../authdirection/authdirection.service';
import { Title } from '@angular/platform-browser';

import { CustomerService } from '../shared/customer.service';
import { SharedlistappService } from '../sharedlistatt/sharedlistapp.service';

@Component({
  selector: 'app-direction',
  templateUrl: './direction.component.html',
  styleUrls: ['./direction.component.css']
})
export class DirectionComponent implements OnInit {
  
ville=["ville","CASABLACA","RABAT","TANGER"];
niveaue=["Niveau Etude","1er annee","2eme annee","3eme annee","4eme annee","5eme annee"];
filiere=["Filiere","Finance","Informatique","Genie civil"];
constructor(private customerService: SharedlistappService,private titleService: Title,public authService: AuthdirectionService) {

  }
  submitted: boolean;
  showSuccessMessage: boolean;
  formControls = this.customerService.form.controls;



  ngOnInit() {
    this.setTitle(' Espace Direction');

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}

  logout() {
    this.authService.signOut();
  }
  onSubmit() {
    this.submitted = true;
    if (this.customerService.form.valid) {
  if ( this.customerService.form.get('$key').value == null) {
        this.customerService.insertCustomer(this.customerService.form.value);
      } else {
        this.customerService.updateCustomer(this.customerService.form.value);
      }
      this.showSuccessMessage = true;
      setTimeout(() => this.showSuccessMessage = false, 3000);
      this.submitted = false;
      this.customerService.form.reset();
      this.customerService.form.setValue({
        $key: null,
        nom: '',
        prenom: '',
        daten: '',
        filiere: '',
        email: '',
        mobile: '',
        pays: '',
        ville: '',
        niveaue: '',
      });
    }
  }

}
