import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeprofComponent } from './listeprof.component';

describe('ListeprofComponent', () => {
  let component: ListeprofComponent;
  let fixture: ComponentFixture<ListeprofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeprofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
