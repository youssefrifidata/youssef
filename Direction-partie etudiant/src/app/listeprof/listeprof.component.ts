import { Component, OnInit } from '@angular/core';
import { SharedprofService } from '../sharedprof/sharedprof.service';

@Component({
  selector: 'app-listeprof',
  templateUrl: './listeprof.component.html',
  styleUrls: ['./listeprof.component.css']
})
export class ListeprofComponent implements OnInit {
  constructor(private customerService: SharedprofService) { }
  customerArray = [];
  showDeletedMessage: boolean;
  searchText: string = "";

  ngOnInit() {
    this.customerService.getCustomers().subscribe(
      list => {
        this.customerArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

  onDelete($key) {
    if (confirm('voulez vous supprimer ?')) {
      this.customerService.deleteCustomer($key);
      this.showDeletedMessage = true;
      setTimeout(() => this.showDeletedMessage = false, 3000);
    }
  }


  filterCondition(customer) {
    return customer.nom.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
  }

}
