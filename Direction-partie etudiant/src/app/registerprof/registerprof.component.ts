import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import {Router} from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthdirectionService } from '../authdirection/authdirection.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-registerprof',
  templateUrl: './registerprof.component.html',
  styleUrls: ['./registerprof.component.css']
})
export class RegisterprofComponent implements OnInit {
  showSuccessMessage: boolean;
  submitted: boolean;
  email:string = '';
  password:string = '';
  itemList: AngularFireList<any>

  constructor(public db:AngularFireDatabase,private fire:AngularFireAuth 
    ,private router: Router,public authService: AuthdirectionService,private titleService: Title) { 
    this.itemList = db.list('compteprofesseur')
  }

  logout() {
    this.authService.signOut();
  }
  myRegister(){
    this.submitted = true;

    this.fire.auth.createUserWithEmailAndPassword(this.email, this.password)
    .then(user =>{
      console.log(this.email, this.password)
      localStorage.setItem('isLoggedIn','true')
      localStorage.setItem('email',this.fire.auth.currentUser.email )
      this.showSuccessMessage = true;
      setTimeout(() => this.showSuccessMessage = false, 3000);
      this.submitted = false;
      this.fire.authState.subscribe(auth=>{
        if(auth){
          localStorage.setItem('uid',auth.uid )
  this.itemList.push({
    email: this.email ,
    uid : auth.uid
  })
  
        }
      })
alert('compte crée')
    }).catch(error=>{
      console.error(error)
    })
  }


  ngOnInit() {
    this.setTitle('Creer un compte pour un professeur ');
  
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}

}
