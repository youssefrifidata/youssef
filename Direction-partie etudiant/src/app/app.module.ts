import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AppComponent } from './app.component';
import { CustomerComponent } from './etudiant/etudiant.component';
import { CustomerListComponent } from './liste-etudiant/liste-etudiant.component';
import { CustomerService } from './shared/customer.service';
import { environment } from '../environments/environment';
import { RouterModule, Routes } from '@angular/router';
import { ProfComponent } from './prof/prof.component';
import { HomeComponent } from './home/home.component';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HttpClientModule } from '@angular/common/http';
import { ListeprofComponent } from './listeprof/listeprof.component';
import { RegisterComponent } from './register/register.component';
import { RegisterprofComponent } from './registerprof/registerprof.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { MatiereService } from './sharedprof/matiere.service';
import { SharedprofService } from './sharedprof/sharedprof.service';
import { UserInfoprofComponent } from './authprof/user-infoprof/user-infoprof.component';
import { UserLoginprofComponent } from './authprof/user-loginprof/user-loginprof.component';
import { AuthprofService } from './authprof/authprof.service';

import { UserLogindirectionComponent } from './authdirection/user-logindirection/user-logindirection.component';
import { UserLoginetudiantComponent } from './authetudiant/user-loginetudiant/user-loginetudiant.component';
import { UserInfoetudiantComponent } from './authetudiant/user-infoetudiant/user-infoetudiant.component';
import { PlanningetudiantComponent } from './planningetudiant/planningetudiant.component';
import { PlannificationComponent } from './plannification/plannification.component';
import { CoursprofComponent } from './coursprof/coursprof.component';
import { CoursetudiantComponent } from './coursetudiant/coursetudiant.component';
import { UserInscriptionetudiantComponent } from './authetudiant/user-inscriptionetudiant/user-inscriptionetudiant.component';
import { ApplistComponent } from './applist/applist.component';
import { SharedlistappService } from './sharedlistatt/sharedlistapp.service';
import { ListeattComponent } from './listeatt/listeatt.component';
import { EtudiantattComponent } from './etudiantatt/etudiantatt.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

// const routes: Routes = [
//   { path: '' , redirectTo: 'home' , pathMatch: 'full' },
//   { path: 'home', component: HomeComponent },
//   {path: 'prof', component: ProfComponent },
//   {path: 'Etudiant', component: CustomerComponent},
//   {path: 'register', component: RegisterComponent},
//   {path: 'registerprof', component: RegisterprofComponent}
// ];

@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    CustomerListComponent,
    ProfComponent,
    HomeComponent,
    ListeprofComponent,
    RegisterComponent,
    RegisterprofComponent,
    routingComponents,
    UserInfoprofComponent,
    UserLoginprofComponent,
    
    UserLogindirectionComponent,
    
    UserLoginetudiantComponent,
    
    UserInfoetudiantComponent,
    
    PlanningetudiantComponent,
    
    PlannificationComponent,
    
    CoursprofComponent,
    
    CoursetudiantComponent,
    
    UserInscriptionetudiantComponent,
    
    ApplistComponent,
    
    ListeattComponent,
    
    EtudiantattComponent,
  ],
  imports: [
    BrowserModule,
    
    ReactiveFormsModule,
    // RouterModule.forRoot(routes, { useHash: false }),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,    AngularFireAuthModule,
    AngularFontAwesomeModule,
    AngularFireStorageModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [CustomerService, SharedlistappService,SharedprofService,MatiereService,AuthprofService],

  bootstrap: [AppComponent]
})
export class AppModule { }
