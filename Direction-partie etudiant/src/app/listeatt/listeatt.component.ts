import { Component, OnInit } from '@angular/core';
import { SharedlistappService } from '../sharedlistatt/sharedlistapp.service';

@Component({
  selector: 'app-listeatt',
  templateUrl: './listeatt.component.html',
  styleUrls: ['./listeatt.component.css']
})
export class ListeattComponent implements OnInit {


  constructor(private customerService: SharedlistappService) { }
  customerArray = [];
  showDeletedMessage: boolean;
  searchText: string = "";

  ngOnInit() {
    this.customerService.getCustomers().subscribe(
      list => {
        this.customerArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

  onDelete($key) {
    if (confirm('voulez vous supprimer ?')) {
      this.customerService.deleteCustomer($key);
      this.showDeletedMessage = true;
      setTimeout(() => this.showDeletedMessage = false, 3000);
    }
  }


  filterCondition(customer) {
    return customer.nom.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
  }

}
