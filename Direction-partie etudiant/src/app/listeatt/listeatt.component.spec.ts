import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeattComponent } from './listeatt.component';

describe('ListeattComponent', () => {
  let component: ListeattComponent;
  let fixture: ComponentFixture<ListeattComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeattComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeattComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
