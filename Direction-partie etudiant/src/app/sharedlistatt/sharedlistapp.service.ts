import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class SharedlistappService {
  constructor(private firebase: AngularFireDatabase) { }
  customerList: AngularFireList<any>;
  addl: AngularFireList<any>;


  form = new FormGroup({
    $key: new FormControl(null),
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    cin: new FormControl('', Validators.required),
    daten: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    matiere: new FormControl('', Validators.required),
    // password: new FormControl('', Validators.required),
    mobile: new FormControl('', [Validators.required, Validators.minLength(8)]),
    // pays: new FormControl(''),
    ville: new FormControl(''),

  });


  getCustomers() {
    this.customerList = this.firebase.list('listatt');
    return this.customerList.snapshotChanges();
  }


  insertCustomer(customer) {
    this.customerList.push({
      nom: customer.nom,
      prenom: customer.prenom,
      cin: customer.cin,
      daten: customer.daten,
      email: customer.email,
      matiere: customer.matiere,
      // password: customer.password,
      mobile: customer.mobile,
      // pays: customer.pays,
      ville: customer.ville,
    });
  }

  populateForm(customer) {
    this.form.setValue(customer);
  }

  updateCustomer(customer) {
    this.customerList.push({
      nom: customer.nom,
      prenom: customer.prenom,
      cin: customer.cin,
      daten: customer.daten,
      email: customer.email,
      matiere: customer.matiere,
      // password: customer.password,
      mobile: customer.mobile,
      // pays: customer.pays,
      ville: customer.ville,
    });
  }

  deleteCustomer($key: string) {
    this.customerList.remove($key);
  }
addlist($key: string) {
  this.addl.push($key);

}

getlist() {
  this.addl = this.firebase.list('etudiant');
  return this.addl.snapshotChanges();
}



}
