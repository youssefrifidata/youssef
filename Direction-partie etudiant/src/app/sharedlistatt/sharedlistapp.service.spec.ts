import { TestBed, inject } from '@angular/core/testing';

import { SharedlistappService } from './sharedlistapp.service';

describe('SharedlistappService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedlistappService]
    });
  });

  it('should be created', inject([SharedlistappService], (service: SharedlistappService) => {
    expect(service).toBeTruthy();
  }));
});
