import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudiantattComponent } from './etudiantatt.component';

describe('EtudiantattComponent', () => {
  let component: EtudiantattComponent;
  let fixture: ComponentFixture<EtudiantattComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtudiantattComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantattComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
