import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningprofComponent } from './planningprof.component';

describe('PlanningprofComponent', () => {
  let component: PlanningprofComponent;
  let fixture: ComponentFixture<PlanningprofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanningprofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
