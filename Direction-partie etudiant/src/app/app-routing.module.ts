import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfComponent } from './prof/prof.component';
import { CustomerComponent } from './etudiant/etudiant.component';
import { RegisterComponent } from './register/register.component';
import { RegisterprofComponent } from './registerprof/registerprof.component';
import { HomeComponent } from './home/home.component';
import { UserLoginprofComponent } from './authprof/user-loginprof/user-loginprof.component';
import { UserInfoprofComponent } from './authprof/user-infoprof/user-infoprof.component';
import { PlanningprofComponent } from './planningprof/planningprof.component';
import { DirectionComponent } from './direction/direction.component';
import { UserLoginetudiantComponent } from './authetudiant/user-loginetudiant/user-loginetudiant.component';
import { UserInfoetudiantComponent } from './authetudiant/user-infoetudiant/user-infoetudiant.component';
import { PlanningetudiantComponent } from './planningetudiant/planningetudiant.component';
import { UserLogindirectionComponent } from './authdirection/user-logindirection/user-logindirection.component';
import { PlannificationComponent } from './plannification/plannification.component';
import { CoursprofComponent } from './coursprof/coursprof.component';
import { CoursetudiantComponent } from './coursetudiant/coursetudiant.component';
import { UserInscriptionetudiantComponent } from './authetudiant/user-inscriptionetudiant/user-inscriptionetudiant.component';
import { EtudiantattComponent } from './etudiantatt/etudiantatt.component';
import { ListeattComponent } from './listeatt/listeatt.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'loginprof', component: UserLoginprofComponent },
  { path: 'userprof', component: UserInfoprofComponent },
  { path: 'home', component: HomeComponent },
  { path: 'planningprof', component: PlanningprofComponent },
  { path: 'planningetudiant', component: PlanningetudiantComponent },
  { path: 'prof', component: ProfComponent },
  { path: 'Etudiant', component: CustomerComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'registerprof', component: RegisterprofComponent },
  { path: 'direction', component: DirectionComponent },
  { path: 'loginetudiant', component: UserLoginetudiantComponent }, 
  { path: 'userinfoetudiant', component: UserInfoetudiantComponent },
  { path: 'logindirection', component: UserLogindirectionComponent },
  { path: 'plannification', component: PlannificationComponent },
  { path: 'coursprof', component: CoursprofComponent },
  { path: 'coursetudiant', component: CoursetudiantComponent },
  { path: 'inscriptionetudiant', component: UserInscriptionetudiantComponent },
  { path: 'etudiantatt', component: EtudiantattComponent },
  { path: 'listeatt', component: ListeattComponent },
];
@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ProfComponent, PlanningetudiantComponent,
  DirectionComponent, PlannificationComponent, PlanningprofComponent, UserLoginetudiantComponent
  , UserLoginprofComponent, CustomerComponent, EtudiantattComponent, UserLogindirectionComponent, UserInscriptionetudiantComponent]