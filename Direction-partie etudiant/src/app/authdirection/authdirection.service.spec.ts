import { TestBed, inject } from '@angular/core/testing';

import { AuthdirectionService } from './authdirection.service';

describe('AuthdirectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthdirectionService]
    });
  });

  it('should be created', inject([AuthdirectionService], (service: AuthdirectionService) => {
    expect(service).toBeTruthy();
  }));
});
