import { Component, OnInit } from '@angular/core';
import { AuthdirectionService } from '../authdirection.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-user-logindirection',
  templateUrl: './user-logindirection.component.html',
  styleUrls: ['./user-logindirection.component.css']
})
export class UserLogindirectionComponent implements OnInit {

 
  isNewUser = true;
  email = '';
  password = '';
  errorMessage = '';
  error: {name: string, message: string} = {name: '', message: ''};

  resetPassword = false;

  constructor(public authService: AuthdirectionService, private titleService: Title, private router: Router) {}

  ngOnInit() {
    this.setTitle('Espace Direction');

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}
  clearErrorMessage() {
    this.errorMessage = '';
    this.error = {name: '', message: ''};
  }

  changeForm() {
    this.isNewUser = !this.isNewUser
  }

  onSignUp(): void {
    this.clearErrorMessage()

    if (this.validateForm(this.email, this.password)) {
      this.authService.signUpWithEmail(this.email, this.password)
        .then(() => {
          this.router.navigate(['/user'])
        }).catch(_error => {
          this.error = _error
          this.router.navigate(['/'])
        })
    }
  }

  onLoginEmail(): void {
    this.clearErrorMessage()

    if (this.validateForm(this.email, this.password)) {
      this.authService.loginWithEmail(this.email, this.password)
        .then(() => this.router.navigate(['../direction']))
        .catch(_error => {
          this.error = _error
          this.router.navigate(['/'])
        })
    }
  }

  validateForm(email: string, password: string): boolean {
    if (email.length === 0) {
      this.errorMessage = 'saisir une adresse electronique'
      return false
    }

    if (password.length === 0) {
      this.errorMessage = 'entrer un mot de passe!'
      return false
    }

    if (password.length < 6) {
      this.errorMessage = 'le mot de passe doit contenir au minimum 6 caracteres!'
      return false
    }

    this.errorMessage = ''

    return true
  }

  isValidMailFormat(email: string) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if ((email.length === 0) && (!EMAIL_REGEXP.test(email))) {
      return false;
    }

    return true;
  }

  sendResetEmail() {
    this.clearErrorMessage()

    this.authService.resetPassword(this.email)
      .then(() => this.resetPassword = true)
      .catch(_error => {
        this.error = _error
      })
  }
}
