import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLogindirectionComponent } from './user-logindirection.component';

describe('UserLogindirectionComponent', () => {
  let component: UserLogindirectionComponent;
  let fixture: ComponentFixture<UserLogindirectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLogindirectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLogindirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
