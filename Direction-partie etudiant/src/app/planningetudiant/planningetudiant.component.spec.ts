import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningetudiantComponent } from './planningetudiant.component';

describe('PlanningetudiantComponent', () => {
  let component: PlanningetudiantComponent;
  let fixture: ComponentFixture<PlanningetudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanningetudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningetudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
