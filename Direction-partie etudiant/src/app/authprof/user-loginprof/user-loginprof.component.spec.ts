import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginprofComponent } from './user-loginprof.component';

describe('UserLoginprofComponent', () => {
  let component: UserLoginprofComponent;
  let fixture: ComponentFixture<UserLoginprofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginprofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
