import { TestBed, inject } from '@angular/core/testing';

import { AuthprofService } from './authprof.service';

describe('AuthprofService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthprofService]
    });
  });

  it('should be created', inject([AuthprofService], (service: AuthprofService) => {
    expect(service).toBeTruthy();
  }));
});
