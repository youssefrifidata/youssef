import { Component, OnInit } from '@angular/core';
import { AuthprofService } from '../authprof.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-user-infoprof',
  templateUrl: './user-infoprof.component.html',
  styleUrls: ['./user-infoprof.component.css']
})
export class UserInfoprofComponent implements OnInit {

  constructor(public authService: AuthprofService,private titleService: Title) {}
 
  ngOnInit() {
    this.setTitle('Espace Professeur');

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}
 
  logout() {
    this.authService.signOut();
  }
}
