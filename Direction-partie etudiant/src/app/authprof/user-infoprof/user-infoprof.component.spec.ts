import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInfoprofComponent } from './user-infoprof.component';

describe('UserInfoprofComponent', () => {
  let component: UserInfoprofComponent;
  let fixture: ComponentFixture<UserInfoprofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInfoprofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfoprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
