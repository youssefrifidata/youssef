import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class SharedprofService {
  constructor(private firebase: AngularFireDatabase) { }
  customerList: AngularFireList<any>;

  form = new FormGroup({
    $key: new FormControl(null),
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    cin: new FormControl('', Validators.required),
    daten: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    matiere: new FormControl('', Validators.required),
    // password: new FormControl('', Validators.required),
    mobile: new FormControl('', [Validators.required, Validators.minLength(8)]),
    // pays: new FormControl(''),
    ville: new FormControl(''),

  });


  getCustomers() {
    this.customerList = this.firebase.list('professeur');
    return this.customerList.snapshotChanges();
  }


  insertCustomer(customer) {
    this.customerList.push({
      nom: customer.nom,
      prenom: customer.prenom,
      cin: customer.cin,
      daten: customer.daten,
      email: customer.email,
      matiere: customer.matiere,
      // password: customer.password,
      mobile: customer.mobile,
      // pays: customer.pays,
      ville: customer.ville,
    });
  }

  populateForm(customer) {
    this.form.setValue(customer);
  }

  updateCustomer(customer) {
    this.customerList.update(customer.$key,
      {
        nom: customer.nom,
        prenom: customer.prenom,
        cin: customer.cin,
        daten: customer.daten,
        email: customer.email,
        matiere: customer.matiere,
        // password: customer.password,
        mobile: customer.mobile,
        // pays: customer.pays,
        ville: customer.ville,
      });
  }

  deleteCustomer($key: string) {
    this.customerList.remove($key);
  }

}
