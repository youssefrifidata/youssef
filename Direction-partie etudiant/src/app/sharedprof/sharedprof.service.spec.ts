import { TestBed, inject } from '@angular/core/testing';

import { SharedprofService } from './sharedprof.service';

describe('SharedprofService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedprofService]
    });
  });

  it('should be created', inject([SharedprofService], (service: SharedprofService) => {
    expect(service).toBeTruthy();
  }));
});
