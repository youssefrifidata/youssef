import { TestBed, inject } from '@angular/core/testing';

import { InscriptionetudiantService } from './inscriptionetudiant.service';

describe('InscriptionetudiantService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InscriptionetudiantService]
    });
  });

  it('should be created', inject([InscriptionetudiantService], (service: InscriptionetudiantService) => {
    expect(service).toBeTruthy();
  }));
});
