import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class InscriptionetudiantService {

  constructor(private firebase: AngularFireDatabase) { }
  customerList: AngularFireList<any>;

  form = new FormGroup({
    $key: new FormControl(null),
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    daten: new FormControl('', Validators.required),
    filiere: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    mobile: new FormControl('', [Validators.required, Validators.minLength(8)]),
    ville: new FormControl(''),
    niveaue: new FormControl('')


  });


  getCustomers() {
    this.customerList = this.firebase.list('etudiant');
    return this.customerList.snapshotChanges();
  }


  insertCustomer(customer) {
    this.customerList.push({
      nom: customer.nom,
      prenom: customer.prenom,
      daten: customer.daten,
      email: customer.email,
      filiere: customer.filiere,
      mobile: customer.mobile,
      ville: customer.ville,
    niveaue: customer.niveaue

    });
  }

  populateForm(customer) {
    this.form.setValue(customer);
  }

  updateCustomer(customer) {
    this.customerList.update(customer.$key,
      {
        // nom: customer.nom,
        // prenom: customer.prenom,
        // cin: customer.cin,
        // daten: customer.daten,
        // filiere: customer.filiere,
        // email: customer.email,
        // password: customer.password,
        // mobile: customer.mobile,
        // pays: customer.pays,
        // ville: customer.ville,
        // niveaue: customer.niveaue

        nom: customer.nom,
        prenom: customer.prenom,
        daten: customer.daten,
        email: customer.email,
        filiere: customer.filiere,
        mobile: customer.mobile,
        ville: customer.ville,
      niveaue: customer.niveaue

      });
  }

  deleteCustomer($key: string) {
    this.customerList.remove($key);
  }

}
