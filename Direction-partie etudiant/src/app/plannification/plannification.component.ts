import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-plannification',
  templateUrl: './plannification.component.html',
  styleUrls: ['./plannification.component.css']
})
export class PlannificationComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.setTitle(' Plannification ');

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}

}
