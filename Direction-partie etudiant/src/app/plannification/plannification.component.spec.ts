import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannificationComponent } from './plannification.component';

describe('PlannificationComponent', () => {
  let component: PlannificationComponent;
  let fixture: ComponentFixture<PlannificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlannificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
