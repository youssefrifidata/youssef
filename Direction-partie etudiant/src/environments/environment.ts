// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {     apiKey: "AIzaSyDryMKG_RrdQsf068qmA1GoNpOiZzUrkFY",
  authDomain: "ecole2-c4171.firebaseapp.com",
  databaseURL: "https://ecole2-c4171.firebaseio.com",
  projectId: "ecole2-c4171",
  storageBucket: "ecole2-c4171.appspot.com",
  messagingSenderId: "583720238369"
}
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
